@extends('layouts.master')

@section('content')
<div class="ml-3 mt-3" >
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Question {{$tanya->id}} </h3>
        </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$tanya->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $tanya->title)}}" placeholder="Enter title">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <input type="text" class="form-control" id="content" name="content" value="{{ old('content', $tanya->content)}}" placeholder="Put your question in detail here">
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>    
                </div>
                    <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
    </div>
</div>
    

@endsection
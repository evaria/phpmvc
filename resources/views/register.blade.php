<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            SanberBook Sign Up Form   
        </title>
    </head>

    <body>
        
    <!--Judul-->
    <div>
        <h1> Buat Account Baru! </h1>
        <h2> Sign Up Form </h2>
    </div>

    <!--Isi Form-->

    <form action=/welcome , method="POST">
        @csrf
        <label for="first_name">First Name:</label><br><br>
        <input type="text" name="first_name">
        <br><br>
        
        <label for="last_name">Last</label> Name:</label><br><br>
        <input type="text" name="last_name">
        <br><br>
        
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other
        <br><br>

        <label>Nationality:</label><br><br>
        <select>
            <option value="idn">Indonesian</option>
            <option value="sg">Singaporean</option>
            <option value="my">Malaysian</option>
            <option value="viet">Vietnamese</option>
        </select>
        <br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language_user" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_user" value="1"> English <br>
        <input type="checkbox" name="language_user" value="2"> Other 
        <br><br>        
        
        <label for="bio_user">Bio:</label>
        <br><br> 
        <textarea cols="30" rows="10" name="bio_user"> </textarea>
        <br>

        <input type="submit" value="Sign Up" > 
        
    </form>
    
</body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function hello(){
        return view('welcome');
         
    }

    public function hello_post(Request $request){
        //dd($request->all());
        $firstname = $request["first_name"];
        $lastname  = $request["last_name"];
        return view('welcome', ["firstname" => $firstname], ["lastname" =>$lastname]);
         
    }
}

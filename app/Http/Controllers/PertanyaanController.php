<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }


    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'content' => 'required'
        ]);
    
        $query = DB::table('pertanyaan')->insert([
            "title" => $request['title'],
            "content" => $request['content']
        ]);
        
        return redirect('/pertanyaan')->with('success','Your question has been submitted successfully');
    }

    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan)->all();
        return view('pertanyaan.index', compact('pertanyaan'));

    }

    public function show($id) {
        $tanya = DB::table('pertanyaan')->where('id', $id)->first();
        //dd($pertanyaan)->all();
        return view('pertanyaan.show', compact('tanya'));

    }

    public function edit($id) {
        $tanya = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('tanya'));

    }

    public function update($id, Request $request) {
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'content' => 'required'
        ]);
        
        $affected = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'],
                        'content' => $request['content']
                    ]);
        return redirect('/pertanyaan')->with('success','Your question has been edited successfully');

    }

    public function destroy($id) {
        $affected = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success','Your question has been deleted successfully');
    }
}
